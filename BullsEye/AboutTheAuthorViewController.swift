//
//  AboutTheAuthorViewController.swift
//  BullsEye
//
//  Created by Boros on 7/20/19.
//  Copyright © 2019 Boros. All rights reserved.
//

import UIKit

class AboutTheAuthorViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }
    
}
