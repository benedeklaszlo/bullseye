//
//  ViewController.swift
//  BullsEye
//
//  Created by Boros on 7/20/19.
//  Copyright © 2019 Boros. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var slider: UISlider?
    
    @IBOutlet weak var startOverButton: UIButton?
    
    @IBOutlet weak var infoButton: UIButton?
    
    @IBOutlet weak var scoreLabel: UILabel?
    
    @IBOutlet weak var roundLabel: UILabel?
    
    @IBOutlet weak var numberToGuessLabel: UILabel?
    
    var currentValue = 0
    var targetValue = 0
    var totalScore = 0
    var round = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    private func initialize() {
        let thumbNormalImage = #imageLiteral(resourceName: "SliderThumb-Normal")
        slider?.setThumbImage(thumbNormalImage, for: .normal)
        
        let thumbHighlightedImage = #imageLiteral(resourceName: "SliderThumb-Highlighted")
        slider?.setThumbImage(thumbHighlightedImage, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft")
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider?.setMinimumTrackImage(trackLeftResizable, for: .normal)
        
        let trackRightImage = #imageLiteral(resourceName: "SliderTrackRight")
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider?.setMaximumTrackImage(trackRightResizable, for: .normal)
        
        if let sliderValue = slider?.value {
            currentValue = Int(sliderValue.rounded())
        }
        startNewRound()
    }
    
    private func startNewRound() {
        round += 1
        targetValue = Int.random(in: 1...100)
        currentValue = 50
        slider?.value = Float(currentValue)
        updateLabels()
    }
    
    private func updateLabels() {
        numberToGuessLabel?.text = String(targetValue)
        roundLabel?.text = String(round)
        scoreLabel?.text = String(totalScore)
    }
    
    private func calculateDifference() -> Int {
        return abs(targetValue - currentValue)
    }
    
    private func calculateScoredPoints() -> Int {
        let difference = calculateDifference()
        var points = 100 - difference
        if targetValue == currentValue {
            points += 100
        } else if difference == 1 {
            points += 50
        }
        return points
    }
    
    private func createTitleText() -> String{
        let difference = calculateDifference()
        var title: String
        if difference == 0 {
            title = "Perfect!"
        } else if difference < 5 {
            title = "You almost had it!"
        } else if difference < 10 {
            title = "Pretty good!"
        } else {
            title = "Not even close..."
        }
        return title
    }
    
    @IBAction func startOver() {
        round = 0
        totalScore = 0
        startNewRound()
    }
    
    @IBAction func showAlert() {
        let scoredPoints = calculateScoredPoints()
        totalScore += scoredPoints
        
        let message = "You scored \(scoredPoints) points"
        let alert = UIAlertController(title: createTitleText(), message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: {
            action in
            self.startNewRound()
        })
        
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func sliderMoved(_ slider: UISlider) {
        currentValue = Int(slider.value.rounded())
    }
    
}
